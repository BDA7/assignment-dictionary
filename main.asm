extern find_word_dict
extern print_string
extern string_length
extern print_newline
extern exit
extern read_word

global _start

section .data
%include "colon.inc"
%include "words.inc"

WELCOME: db "Key: ", 0
FOUND:  db "Value: ", 0
KEY_NOT:  db "Value with key '", 0
NOT_FOUND:  db "' not found", 0
NOT_READ: db "Error while reading key.", 0

%define BUF_SIZE 256

section .text
_start:
    mov rdi, WELCOME
    call print_string
 
    sub rsp, BUF_SIZE
    mov rdi, rsp
    mov rsi, BUF_SIZE
    call read_word
    test rax, rax
    jz .read_err

    push rsp
    mov rdi, [rsp]
    mov rsi, nextElement
    call find_word_dict
    test rax, rax
    jz .not_found
.found:
    pop rdi
    add rax, 8  
    push rax
    call string_length
    inc rax 
    add [rsp], rax 
    mov rdi, FOUND
    call print_string
    pop rdi
    call print_string
    call print_newline
    call exit
.not_found:
    mov rdi, KEY_NOT
    call print_string
    pop rdi
    call print_string
    mov rdi, NOT_FOUND
    call print_string
    call print_newline
    mov rdi, -1
    call exit
.read_err:
    mov rdi, NOT_READ
    call print_string
    call print_newline
    mov rdi, -1
    call exit
